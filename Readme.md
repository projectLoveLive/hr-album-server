# スクフェスAC HRアルバム(Web backend apis) 

### about this repository
* [hr-album-client](https://gitlab.com/projectLoveLive/hr-album-client)へAPIを提供するWebバックエンドアプリ。

### branches
* 方針
  * master, dev : 本開発(公開)用
  * test-～ : 公開を前提としないソースのコミット用
  
### 雛形ソース(SpringInitializr)
[このURL](https://start.spring.io/starter.zip?type=maven-project&bootVersion=1.5.3.RELEASE&baseDir=hr-album-server&groupId=pink.nico&artifactId=hr-album-server&name=hr-album-server&description=School+idol+festival+Arcade+HR+Album+Web+Backend.&packageName=pink.nico&packaging=jar&javaVersion=1.8&language=java&autocomplete=&generate-project=&style=security&style=devtools&style=lombok&style=web&style=restdocs&style=thymeleaf&style=mybatis&style=postgresql&style=actuator)にて取得。

### !Notice!
* Kotlinとの混在ソース。
  * ref: https://kotlinlang.org/docs/reference/using-maven.html